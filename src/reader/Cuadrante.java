package reader;
//Representa un cuadrante del mapa si tiene dos latitudes y dos longitudes
//Si solo tiene una latitud y una longitud es una ubicación en el mapa
public class Cuadrante implements Comparable<Cuadrante> {

	private double latSup;

	private double latInf;

	private double lonSup;

	private double lonInf;

	public Cuadrante(double lats, double lati, double lons, double loni)
	{
		latSup = lats;
		latInf = lati;
		lonSup = lons;
		lonInf = loni;
	}


	public double getlatSup()
	{
		return latSup;
	}

	public double getLatInf()
	{
		return latInf;
	}

	public double getlonSup()
	{
		return lonSup;
	}

	public double getLonInf()
	{
		return lonInf;
	}

	public void setLonInf(double sec)
	{
		lonInf = sec;
	}

	public void setLonSup(double sec)
	{
		lonSup = sec;
	}

	public void setlatInf(double sec)
	{
		latInf = sec;
	}

	public void setlatSup(double sec)
	{
		latSup = sec;
	}

	@Override
	public int compareTo(Cuadrante arg0) {
		double latI = arg0.getLatInf();
		double latS = arg0.getlatSup();
		double lonI = arg0.getLonInf();
		double lonS = arg0.getlonSup();
		
		if(Double.toString(this.latInf).equals(Double.toString(latI)) && Double.toString(this.latSup).equals(Double.toString(latS)) && Double.toString(this.lonInf).equals(Double.toString(lonI)) && Double.toString(this.lonSup).equals(Double.toString(lonS)))
			return 0;
		else
			return -1;
	}
	
	public String ToString()
	{
		return "LatInf: "+ Double.toString(this.latInf) +"\n" +
			"LonInf: " +Double.toString(this.lonInf) + "\n" +
			"LatSup: " + Double.toString(this.latSup) + "\n" +
			"LonSup: " +Double.toString(this.lonSup) + "\n" +
			"---------------------------------";

	}
	

}
