package reader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import model.data_structures.Graph;

public class lector {
	private  Graph<String, Node, Double> grafo;
	public lector(Graph g)
	{
		grafo = g;
		leer();
	}

	public void leer(){
		JSONParser parser = new JSONParser();

		try {
			Object obj = parser.parse(new FileReader("./finalGraph.json"));

			JSONArray jsonObject = (JSONArray) obj;

			for(int o=0; o < jsonObject.size(); o++){
				JSONObject search = (JSONObject)jsonObject.get(o);
				
				String id = (String) search.get("id");
				double lat;
				double lon;
				
				if(search.get("lat") instanceof Long)
					lat = ((Long)search.get("lat")).doubleValue();
				else
					lat = (Double) search.get("lat");
				
				if(search.get("lon") instanceof Long)
					lon = ((Long)search.get("lon")).doubleValue();
				else
					lon = (Double) search.get("lon");
				
				Node node = new Node(id, lat,lon);

				JSONArray adj = (JSONArray) search.get("adj");

				for (int i = 0; i < adj.size(); i++) {
					String bike = (String)adj.get(i);


				}

				JSONArray infractions = (JSONArray) search.get("infractions");

				for (int i = 0; i < infractions.size(); i++) {
					String objectid = (String)infractions.get(i);
					node.add(Integer.parseInt(objectid));

				}
				grafo.addVertex(id, node);
			}
			
			Object obj2 = parser.parse(new FileReader("./finalGraph.json"));
			
			JSONArray jsonObject2 = (JSONArray) obj2;
			
			for(int o=0; o < jsonObject2.size(); o++){
				JSONObject search = (JSONObject)jsonObject2.get(o);
				
				String id = (String) search.get("id");



				JSONArray adj = (JSONArray) search.get("adj");

				for (int i = 0; i < adj.size(); i++) {
					String idd2 = (String)adj.get(i);
					Node jj = grafo.getInfoVertex(id);
					Node hh = grafo.getInfoVertex(idd2);
					double harv = calcularHarvesine(hh.getLat(),jj.getLat(),hh.getlon(),jj.getlon());
					grafo.addEdge(id,idd2,harv);


				}

				JSONArray infractions = (JSONArray) search.get("infractions");

			}
		} 
		catch (FileNotFoundException h) {
			h.printStackTrace();
		} catch (IOException t) {
			t.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 

	}
	
	public  double calcularHarvesine(double lat1, double lat2, double lon1, double lon2)
	{
		double restaLat = lat2-lat1;
		double restaLon = lon2-lon1;
		double difLatitud =Math.toRadians(restaLat);
		double difLongitud= Math.toRadians(restaLon);

		double a = Math.pow(Math.sin(difLatitud/2),2) +
				Math.cos(Math.toRadians(lat1))*
				Math.cos(Math.toRadians(lat2))*
				Math.pow(Math.sin(difLongitud/2),2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		return c*6371.01;
	}
}