package reader;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.teamdev.jxmaps.LatLng;
import model.data_structures.Arco;
import model.data_structures.BFS;
import model.data_structures.BusquedaSecuencialST;
import model.data_structures.Graph;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHash;
import model.data_structures.Vertice;
import model.vo.VOMovingViolation;

public class Manager
{
	private  Graph<String, Node,Double> grafo;

	private  SeparateChainingHash<Integer,VOMovingViolation> lista;

	private  SeparateChainingHash<Cuadrante,Queue> tablita;






	public Manager()
	{
		grafo = new Graph<String, Node, Double>(745747);
	}

	public void cargar()
	{
		tablita = new SeparateChainingHash<Cuadrante,Queue>(500000);
		grafo = new Graph<String, Node, Double>(745747);
		//		loadMovingViolations();  
		lector leer = new lector(grafo);
		System.out.println(grafo.V());
		System.out.println(grafo.E());


		try {
			requerimiento5b(38.9251100, -76.7363700, 38.8751100, -77.0363700, 4, 3 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Leer los datos de las infracciones de los archivos. Cada infraccion debe ser Comparable para ser usada en los ordenamientos.
	 * Todas infracciones (MovingViolation) deben almacenarse en una Estructura de Datos (en el mismo orden como estan los archivos)
	 * A partir de estos datos se obtendran muestras para evaluar los algoritmos de ordenamiento
	 * @return numero de infracciones leidas 
	 */
	public void loadMovingViolations() {
		lista = new SeparateChainingHash<Integer,VOMovingViolation>(1263696);
		String pth;


		for(int j=1; j< 13; j++){
			pth = "./data/" + Integer.toString(j) +".csv";

			Path myPath = Paths.get(pth);
			try (BufferedReader br = Files.newBufferedReader(myPath)){


				HeaderColumnNameMappingStrategy<VOMovingViolation> strategy = new HeaderColumnNameMappingStrategy<>();
				strategy.setType(VOMovingViolation.class);
				CsvToBean csvToB = new CsvToBeanBuilder(br).withType(VOMovingViolation.class).withSeparator(';').withMappingStrategy(strategy).withIgnoreLeadingWhiteSpace(true).build();


				for(Iterator<VOMovingViolation> i = csvToB.parse().iterator();i.hasNext(); ){

					VOMovingViolation at = i.next();
					lista.put(at.getOBJECTID(), at);

				}

			}

			catch (FileNotFoundException e1) {

			} 
			catch (IOException e) {

			}			
		}

		System.out.println("N�mero de infracciones: " +  lista.getAux());
	}



	/////////////DE AQUI PA ABAJO NO BORRES XFAS DEPRONTO SIRVA SINO LO ELIMINO


	/**
	 * Encuentra el nodo cuya distancia a una infracci�n es la m�nima comparada con los dem�s nodos de una cola
	 * @param cola, cola que contiene los nodos a comparar
	 * @param g, infracci�n con la que se va a comparar
	 * @return devuelve el nodo
	 */
	public  Node darNodoDistanciaMinima(Queue cola, double lat1, double lon1)
	{
		Node n = null;

		Iterator<Node> iter = cola.iterator();
		if(cola.size >0){
			n = iter.next();
			double lat2 = n.getLat();
			double lon2 = n.getlon();
			double distancia = calcularHaversine(lat1, lat2, lon1, lon2);
			while(iter.hasNext())
			{
				Node n2 = iter.next();
				double lat22 = n2.getLat();
				double lon22 = n2.getlon();
				double distancia2 = calcularHaversine(lat1, lat22, lon1, lon22);

				if(distancia2 < distancia)
				{
					n = n2;
					distancia = distancia2;
				}
			}
		}
		return n;
	}


	/**
	 * Calcula la distancia harvesiana entre dos localizaciones
	 * @param lat1, latitud del primer elemento
	 * @param lat2, latitud del segundo elemento
	 * @param lon1, longitud del primer elemento
	 * @param lon2, longitud del segundo elemento
	 * @return, un double con la distancia harvesiana entre las dos localizaciones 
	 */
	public  double calcularHaversine(double lat1, double lat2, double lon1, double lon2)
	{
		double restaLat = lat2-lat1;
		double restaLon = lon2-lon1;
		double difLatitud =Math.toRadians(restaLat);
		double difLongitud= Math.toRadians(restaLon);

		double a = Math.pow(Math.sin(difLatitud/2),2) +
				Math.cos(Math.toRadians(lat1))*
				Math.cos(Math.toRadians(lat2))*
				Math.pow(Math.sin(difLongitud/2),2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		return c*6371.01;
	}


	public void graficarMapa()
	{

		final Mapa example= new Mapa("test");
		double u= 38.8951100;
		double v = -77.0363700;
		example.generateMarker(new LatLng(u,v));
		u = u+ 0.012;
		v = v+ 0.015;
		example.generateMarker(new LatLng(u,v));
		u = u+ 0.012;
		v = v+ 0.015;
		example.generateMarker(new LatLng(u,v));

		SeparateChainingHash<String, Vertice> ts =  grafo.vertices();
		Iterator<String> k = ts.keys();
		int i=0;

		while(k.hasNext() )
		{
			String str = k.next();
			Vertice<String,Node> vr = ts.get(str);
			Node nod = vr.darValor();

			//		Dibuja un circulo por cada nodo
			double lat = nod.getLat();
			double lon = nod.getlon();

			example.generateCircle(new LatLng(lat, lon), "#000000");		

			//		Dibuja cada arco de cada v�rtice, en caso de que exista
			//			Iterator<Arco> itrAdyacentes = vr.adyacentes();
			//			while(itrAdyacentes.hasNext())
			//			{
			//				Arco arc = itrAdyacentes.next();
			//
			//				if(arc != null){
			//					String verrtIni = (String)arc.darVerticeIni();
			//					String verrtFin = (String)arc.darVerticeFin();
			//					Node nod2 = (Node) ((Vertice)ts.get(verrtIni)).darValor();
			//					Node nod3 = (Node) ((Vertice)ts.get(verrtFin)).darValor();
			//					example.generateSimplePath(new LatLng(nod2.getLat(), nod2.getlon()), new LatLng(nod3.getLat(), nod3.getlon()));
			//				}
			//
			//			}
			i++;
		}

		graficarInfracciones(example);

	}

	public void graficarInfracciones(Mapa pMapa)
	{
		Iterator<Integer> k = lista.keys();
		while(k.hasNext())
		{
			VOMovingViolation f = lista.get(k.next());
			String w = f.getLong();
			String v = f.getLat();
			if(v != null){
				v = v.replace(',','.');
				if(w != null){
					w = w.replace(',','.');
					pMapa.generateCircle(new LatLng(Double.parseDouble(v), Double.parseDouble(w)), "#FF00FF");
					System.out.println("ya carg� uno");
				}
			}
		}
	}


	//////////////REQUERIMIENTOS DEL PROYECTO

	/**
	 * Requerimiento 4B
	 * @throws Exception 
	 */
	public void requerimiento4b () throws Exception
	{
		SeparateChainingHash<String, Vertice> ts =  grafo.vertices();
		double tama�o = ts.getN();
		int random = (int)(Math.random() * tama�o + 0);
		BusquedaSecuencialST g =ts.geti(random);
		int random2 = (int)(Math.random() * tama�o + 0);
		BusquedaSecuencialST i =ts.geti(random2);

		while(g == null || i == null || random == random2){
			random = (int)(Math.random() * tama�o + 0);
			g =ts.geti(random);
			random2 = (int)(Math.random() * tama�o + 0);
			i =ts.geti(random2);
		}


		Iterator<String> itera = g.keys();
		String id = itera.next();
		System.out.println("id : " + id);


		Iterator<String> itera2 = i.keys();
		String id2 = itera2.next();
		System.out.println("id2 : " + id2);

		BFS<String> bff = new BFS(grafo, id);
		if(bff.hasPathTo(id2))
		{
			graficarCamino(bff.getPath(id,id2),id,id2);
		}
		else
		{
			System.out.println("No hay camino entre ambos puntos, intente de nuevo");
		}

	}

	public void graficarCamino(LinkedList<String> lista, String iddd, String iddd2)
	{

		final Mapa mapa= new Mapa("test");
		Iterator<String> iterador = lista.iterator();

		System.out.println("El n�mero de v�rtices del camino m�s corto es: "+ lista.getSize());

		double dst = 0.0;
		int i = 1;
		String id = iterador.next();
		Node n = grafo.getInfoVertex(id);

		while(iterador.hasNext())
		{
			String id2 = iterador.next();
			Node n2 = grafo.getInfoVertex(id2);
			double infoArc = calcularHaversine(n.getLat(), n2.getLat(), n.getlon(), n2.getlon());
			mapa.generateSimplePath(new LatLng(n.getLat(),n.getlon()), new LatLng(n2.getLat(),n2.getlon()));
			n = n2;
			dst += infoArc;
		}

		Node idd = grafo.getInfoVertex(iddd);
		mapa.generateMarker(new LatLng(idd.getLat(),idd.getlon()));
		Node idd2 = grafo.getInfoVertex(iddd2);
		mapa.generateMarker(new LatLng(idd2.getLat(),idd2.getlon()));
		System.out.println("La distancia en km es: " + dst + " km");


	}


	/**
	 * Requerimiento 5B
	 * @throws Exception 
	 */
	public Node[][] requerimiento5b (double latmax, double lonmax, double latmin, double lonmin, int n, int m ) throws Exception
	{
		Node[][] arreglo = new Node[n][m];
		if(n < 2 || m < 2)
			throw new Exception("N y M tienen que ser mayores o iguales que 2");

		else
		{
			Queue<LatLng> intersecciones = new Queue<LatLng>();
			final Mapa example= new Mapa("test");

			double distColum = Math.abs((Math.abs(lonmax)- Math.abs(lonmin))/ (m-1));   
			double distFilas = (Math.abs(latmax)- Math.abs(latmin))/ (n-1);

			for(int i = 0; i < m;i++){
				LatLng pnt = new LatLng(latmin, lonmin+ i*distColum);
				LatLng pnt2 = new LatLng(latmax, lonmin+ i*distColum);
				example.generateSimplePath(pnt, pnt2);	
			}

			for(int j=0; j< n; j++)
			{
				LatLng pnt0 = new LatLng(latmin + j*distFilas, lonmin);
				LatLng pnt02 = new LatLng(latmin + j*distFilas, lonmax);
				example.generateSimplePath(pnt0, pnt02);
			}

			for(int k =0; k < n ; k++)
			{
				double fila = latmin + k*distFilas;
				for(int h=0; h <m;h++)
				{
					double columna = lonmin + h*distColum;
					LatLng pht = new LatLng(fila,columna);
					intersecciones.enqueue(pht);
				}
			}

			int i =0;
			int j = 0;
			while(!intersecciones.isEmpty() && i<n)
			{
				LatLng uu = intersecciones.dequeue();
				example.generateMarker(uu);
				Queue cola = dividir2(latmax, lonmax, latmin, lonmin);
				if(cola != null){

					Node elquees = darNodoDistanciaMinima(cola,uu.getLat(), uu.getLng());
					System.out.println("Vertice m�s cercano a : " + uu.toString() + " es " + elquees.toString() + " con Id: " + elquees.getId());
					example.generateMarker(new LatLng(elquees.getLat(),elquees.getlon()));
					if(j >= m){
						i++;
						j=0;
					}
					arreglo[i][j]= elquees;


					j++;

				}
				else
				{
					System.out.println("mierda :C");
				}
			}
//			requerimiento8c(arreglo,n,m, example);

		}
		return arreglo;
	}

	public Queue<Node> dividir2(double latmax, double lonmax, double latmin, double lonmin)
	{
		SeparateChainingHash<String, Vertice> ts =  grafo.vertices();
		Iterator<String> k = ts.keys();
		Queue<Node> hh = new Queue<Node>();
		while(k.hasNext() )
		{
			Node node = grafo.getInfoVertex(k.next());
			if(node.getLat() <= latmax && node.getLat() >= latmin && node.getlon() <= lonmax && node.getlon() >= lonmin)
			{
				hh.enqueue(node);
			}
		}
		return hh;
	}


	
	
	/**
	 * Requerimiento 8c
	 * @param arr 
	 * @param n
	 * @param m
	 */
	public void requerimiento8c(Node[][] arr, int n,int m, Mapa example)
	{
		if(arr == null)
		{
			System.out.println("Por favor ejecute el requerimiento no. 5 antes de este");
		}
		else
		{
			if(m>=n)
			{
				int j;
				boolean k;
				boolean retro = true;
				boolean y = false;
				for(int i=0; i <n; i++)
				{
					retro = !retro;
					if(retro == false){
						if(y)
						{
							Node n1 = arr[i-1][0];
							Node n2 = arr[i][0];
							//Caminito 
							example.generateSimplePath(new LatLng(n1.getLat(),n1.getlon()),new LatLng(n2.getLat(),n2.getlon()));
						}
						j=1;
						k = j<m;

					}
					else{
						y = true;
						j = m-1;
						k = j>0;
						Node n1 = arr[i-1][j];
						Node n2 = arr[i][j];
						//Caminito
						example.generateSimplePath(new LatLng(n1.getLat(),n1.getlon()),new LatLng(n2.getLat(),n2.getlon()));
					}

					while(k){

						Node n1;
						Node n2;
						if(retro == false){
							n1 = arr[i][j-1];
							n2 = arr[i][j];
							j++;
							k = j<m;
						}

						else
						{
							n1 = arr[i][j];
							n2 = arr[i][j-1];
							j--;
							k = j>0;
						}

						//caminito
						example.generateSimplePath(new LatLng(n1.getLat(),n1.getlon()),new LatLng(n2.getLat(),n2.getlon()));

					}
				}
			}
			else
			{
				int j;
				boolean k;
				boolean retro = true;
				boolean y = false;
				for(int i=0; i <m; i++)
				{
					retro = !retro;
					if(retro == false){
						if(y)
						{
							Node n1 = arr[0][i-1];
							Node n2 = arr[0][i];
							//Caminito 
							example.generateSimplePath(new LatLng(n1.getLat(),n1.getlon()),new LatLng(n2.getLat(),n2.getlon()));
						}
						j=1;
						k = j<n;

					}
					else{
						y = true;
						j = n-1;
						k = j>0;
						Node n1 = arr[j][i-1];
						Node n2 = arr[j][i];
						//Caminito
						example.generateSimplePath(new LatLng(n1.getLat(),n1.getlon()),new LatLng(n2.getLat(),n2.getlon()));
					}

					while(k){
						Node n1;
						Node n2;
						if(retro == false){
							n1 = arr[j-1][i];
							n2 = arr[j][i];
							j++;
							k = j<n;
						}

						else
						{
							n1 = arr[j][i];
							n2 = arr[j-1][i];
							j--;
							k = j>0;
						}

						//caminito
						example.generateSimplePath(new LatLng(n1.getLat(),n1.getlon()),new LatLng(n2.getLat(),n2.getlon()));

					}
				}
			}
		}
	}
}
