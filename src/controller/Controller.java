package controller;

import java.util.Iterator;

import java.util.PriorityQueue;
import java.util.Scanner;

import com.teamdev.jxmaps.LatLng;

import comparators.ByDistance;
import comparators.ByNum;
import model.data_structures.Arco;
import model.data_structures.BFS;
import model.data_structures.BusquedaSecuencialST;
import model.data_structures.CC;
import model.data_structures.Graph;
import model.data_structures.LinkedList;
import model.data_structures.MaxHeapCP;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHash;
import model.data_structures.Vertice;
import reader.Cuadrante;
import model.vo.VOMovingViolation;
import reader.Cuadrante;
import reader.Mapa;

import reader.Node;
import reader.Way;
import reader.lector;
import view.MovingViolationsManagerView;

public class Controller {

	// Componente vista (consola)
	private MovingViolationsManagerView view;

	private Vertice mayor;

	//TODO Definir los atributos de estructuras de datos del modelo del mundo del proyecto
	private  Graph<String, Node,Double> grafo;

	private  SeparateChainingHash<Integer,VOMovingViolation> lista;

	/**
	 * Metodo constructor
	 */
	public Controller()
	{
		view = new MovingViolationsManagerView();
	}

	/**
	 * Metodo encargado de ejecutar los  requerimientos segun la opcion indicada por el usuario
	 */
	public void run(){

		long startTime;
		long endTime;
		long duration;

		Scanner sc = new Scanner(System.in);
		boolean fin = false;


		while(!fin){
			view.printMenu();

			int option = sc.nextInt();
			int idVertice1 = 0;
			int idVertice2 = 0;


			switch(option){

			case 0:
				String RutaArchivo = "";
				view.printMessage("Escoger el grafo a cargar: (1) Downtown  o (2)Ciudad Completa.");
				int ruta = sc.nextInt();
				if(ruta == 1)
					RutaArchivo = "./docs/downtown.json"; //TODO Dar la ruta del archivo de Downtown
				else

					//RutaArchivo = "./docs/grafo.json"; //TODO Dar la ruta del archivo de la ciudad completa
				
					cargar(); //TODO Dar la ruta del archivo de la ciudad completa

				startTime = System.currentTimeMillis();
				loadJSON(RutaArchivo);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				// TODO Informar el total de vértices y el total de arcos que definen el grafo cargado
				break;

				
			case 1:

				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.nextInt();


				startTime = System.currentTimeMillis();
				caminoCostoMinimoA1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/* 
				TODO Consola: Mostrar el camino a seguir con sus vértices (Id, Ubicación Geográfica),
				el costo mínimo (menor cantidad de infracciones), y la distancia estimada (en Km).

				TODO Google Maps: Mostrar el camino resultante en Google Maps 
				(incluyendo la ubicación de inicio y la ubicación de destino).
				 */
				break;

			case 2:
				view.printMessage("2A. Consultar los N v�rtices con mayor n�mero de infracciones. Ingrese el valor de N: ");
				int n = sc.nextInt();


				startTime = System.currentTimeMillis();
				mayorNumeroVerticesA2(n);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/* 
				TODO Consola: Mostrar la informacion de los n vertices 
				(su identificador, su ubicación (latitud, longitud), y el total de infracciones) 
				Mostra el número de componentes conectadas (subgrafos) y los  identificadores de sus vertices 

				TODO Google Maps: Marcar la localización de los vértices resultantes en un mapa en
				Google Maps usando un color 1. Destacar la componente conectada más grande (con
				más vértices) usando un color 2. 
				 */
				break;

			case 3:						
				startTime = System.currentTimeMillis();
				try {
					requerimiento1b();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");

				/*
				   TODO Consola: Mostrar  el camino a seguir, informando
					el total de vértices, sus vértices (Id, Ubicación Geográfica) y la distancia estimada (en Km).

				   TODO Google Maps: Mostre el camino resultante en Google Maps (incluyendo la
					ubicación de inicio y la ubicación de destino).
				 */
				break;

			case 4:		
				double lonMin;
				double lonMax;
				view.printMessage("Ingrese la longitud minima (Ej. -87,806): ");
				lonMin = sc.nextDouble();
				view.printMessage("Ingrese la longitud m�xima (Ej. -87,806): ");
				lonMax = sc.nextDouble();

				view.printMessage("Ingrese la latitud minima (Ej. 44,806): ");
				double latMin = sc.nextDouble();
				view.printMessage("Ingrese la latitud m�xima (Ej. 44,806): ");
				double latMax = sc.nextDouble();

				view.printMessage("Ingrese el n�mero de columnas");
				int columnas = sc.nextInt();
				view.printMessage("Ingrese el n�mero de filas");
				int filas = sc.nextInt();


				startTime = System.currentTimeMillis();
				try {
					requerimiento5b(latMax,latMin,lonMax,lonMin,filas,columnas);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar el número de vértices en el grafo
					resultado de la aproximación. Mostar el identificador y la ubicación geográfica de cada
					uno de estos vértices. 

				   TODO Google Maps: Marcar las ubicaciones de los vértices resultantes de la
					aproximación de la cuadrícula en Google Maps.
				 */
				break;

			case 5:

				startTime = System.currentTimeMillis();
				arbolMSTKruskalC1();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar los vértices (identificadores), los arcos incluidos (Id vértice inicial e Id vértice
					final), y el costo total (distancia en Km) del árbol.

				   TODO Google Maps: Mostrar el árbol generado resultante en Google Maps: sus vértices y sus arcos.
				 */

				break;

			case 6:

				startTime = System.currentTimeMillis();
				arbolMSTPrimC2();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar los vértices (identificadores), los arcos incluidos (Id vértice inicial e Id vértice
				 	final), y el costo total (distancia en Km) del árbol.

				   TODO Google Maps: Mostrar el árbol generado resultante en Google Maps: sus vértices y sus arcos.
				 */
				break;

			case 7:

				startTime = System.currentTimeMillis();
				req8c(null);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar de cada camino resultante: su secuencia de vértices (identificadores) y su costo (distancia en Km).

				   TODO Google Maps: Mostrar los caminos de costo mínimo en Google Maps: sus vértices
					y sus arcos. Destaque el camino más largo (en distancia) usando un color diferente
				 */
				break;

			case 8:
				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				String idVertice = sc.next();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				String idVerticee2 = sc.next();

				startTime = System.currentTimeMillis();
				caminoMasCortoC4(idVertice, idVerticee2,0);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar del camino resultante: su secuencia de vértices (identificadores), 
				   el total de infracciones y la distancia calculada (en Km).

				   TODO Google Maps: Mostrar  el camino resultante en Google Maps: sus vértices y sus arcos.	  */
				break;

			case 9: 	
				fin = true;
				sc.close();
				break;
			}
		}
	}

	public void cargar()
	{
		grafo = new Graph<String, Node, Double>(745747);
		//		loadMovingViolations();  
		lector leer = new lector(grafo);
		System.out.println(grafo.V());
		System.out.println(grafo.E());
	}
	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia


	private void loadJSON(String rutaArchivo) {

		lector lector = new lector(grafo);


	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 1A: Encontrar el camino de costo m�nimo para un viaje entre dos ubicaciones geogr�ficas.
	 * @param idVertice2 
	 * @param idVertice1 
	 * @return 
	 * @return 
	 */
	public LinkedList<String> caminoCostoMinimoA1(int idVertice1, int idVertice2) {

		SeparateChainingHash<String, Integer> distTo = new SeparateChainingHash<String, Integer>(grafo.V());
		SeparateChainingHash<String, String> edgeTo = new SeparateChainingHash<String, String>(grafo.V());
		MaxHeapCP<String> pq = new MaxHeapCP<String>(grafo.V(), new ByNum(grafo, false));

		SeparateChainingHash<String, Vertice> vertices = grafo.vertices();
		Iterator iterador = vertices.keys();

		distTo.put(idVertice1+"", 0);

		while(iterador.hasNext())
		{
			String vertice = (String) iterador.next();
			if(!(vertice.equals(""+idVertice1)))
			{
				distTo.put(vertice, Integer.MAX_VALUE);
			}

		}


		pq.agregar(""+idVertice1);


		while (!pq.esVacia()) {
			String v = pq.delMax();

			Iterator<String> iter = grafo.adj(v);

			while(iter.hasNext())
			{

				String w =  (String) grafo.vertices().get(iter.next()).getArco(v).darVerticeFin();
				int peso = grafo.getInfoVertex(w).getInfraccionesCercanas().getSize();
				if (distTo.get(w) > (distTo.get(v) + peso)) {

					distTo.replace(w, (distTo.get(v) + peso));

					edgeTo.put(v, w);
					pq.agregar(w);
				}
			}

		}

		LinkedList<String> path = new LinkedList<String>();

		boolean cond = true;

		for (String h = ""+idVertice2; cond ; h = edgeTo.get(h)) {
			if(h.equals(""+idVertice1))
			{
				cond = false;
			}else{

				path.add(h);
			}
		}
		return path;


	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 2A: Determinar los n v�rtices con mayor n�mero de infracciones. Adicionalmente identificar las
	 * componentes conectadas (subgrafos) que se definan �nicamente entre estos n v�rtices
	 * @param  int n: numero de vertices con mayor numero de infracciones  
	 * @return 
	 */
	public LinkedList<Vertice> mayorNumeroVerticesA2(int n) {

		SeparateChainingHash<String, Vertice> vert = grafo.vertices();
		Iterator<String> iterados = vert.keys();
		MaxHeapCP<String> vertices = new MaxHeapCP<String>(grafo.V(), new ByNum(grafo, true));
		LinkedList<Vertice> resp = new LinkedList<Vertice>();
		while(iterados.hasNext())
		{
			String vertice = iterados.next();

			vertices.agregar(vertice);
		}

		for(int i = 0; i<n; i++)
		{
			resp.add(vert.get(vertices.delMax()));
		}

		CC conected = new CC(grafo);

		int temp = 0;
		Vertice mayorvert = null;

		for(int i = 0; i< resp.getSize();i++)
		{
			int act = conected.size(Integer.parseInt((String) resp.getElement(i).darLLave()));

			if(act>temp)
			{
				temp = act;
				mayorvert =  resp.getElement(i);
			}
		}

		mayor = mayorvert;

		return resp;
	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 1B: Encontrar el camino m�s corto para un viaje entre dos ubicaciones geogr�ficas 
	 * @param idVertice2 
	 * @param idVertice1 
	 */
	public void requerimiento1b() throws Exception
	{
		SeparateChainingHash<String, Vertice> ts =  grafo.vertices();
		double tama�o = ts.getN();
		int random = (int)(Math.random() * tama�o + 0);
		BusquedaSecuencialST g =ts.geti(random);
		int random2 = (int)(Math.random() * tama�o + 0);
		BusquedaSecuencialST i =ts.geti(random2);

		while(g == null || i == null || random == random2){
			random = (int)(Math.random() * tama�o + 0);
			g =ts.geti(random);
			random2 = (int)(Math.random() * tama�o + 0);
			i =ts.geti(random2);
		}


		Iterator<String> itera = g.keys();
		String id = itera.next();
		System.out.println("id : " + id);


		Iterator<String> itera2 = i.keys();
		String id2 = itera2.next();
		System.out.println("id2 : " + id2);

		BFS<String> bff = new BFS(grafo, id);
		if(bff.hasPathTo(id2))
		{
			graficarCamino(bff.getPath(id,id2),id,id2);
		}
		else
		{
			System.out.println("No hay camino entre ambos puntos, intente de nuevo");
		}

	}

	public void graficarCamino(LinkedList<String> lista, String iddd, String iddd2)
	{

		final Mapa mapa= new Mapa("test");
		Iterator<String> iterador = lista.iterator();

		System.out.println("El n�mero de v�rtices del camino m�s corto es: "+ lista.getSize());

		double dst = 0.0;
		int i = 1;
		String id = iterador.next();
		Node n = grafo.getInfoVertex(id);

		while(iterador.hasNext())
		{
			System.out.println("id: "+ n.getId() + " | Ubicaci�n geogr�fica :  lat :" + n.getLat() + ", lon: " + n.getlon());

			String id2 = iterador.next();
			Node n2 = grafo.getInfoVertex(id2);
			double infoArc = calcularHaversine(n.getLat(), n2.getLat(), n.getlon(), n2.getlon());
			mapa.generateSimplePath(new LatLng(n.getLat(),n.getlon()), new LatLng(n2.getLat(),n2.getlon()));
			n = n2;
			dst += infoArc;
		}

		Node idd = grafo.getInfoVertex(iddd);
		mapa.generateMarker(new LatLng(idd.getLat(),idd.getlon()));
		Node idd2 = grafo.getInfoVertex(iddd2);
		mapa.generateMarker(new LatLng(idd2.getLat(),idd2.getlon()));
		System.out.println("La distancia en km es: " + dst + " km");


	}
	/**
	 * Requerimiento 5B
	 * @throws Exception 
	 */
	public LinkedList<Node> requerimiento5b (double latmax, double lonmax, double latmin, double lonmin, int n, int m ) throws Exception
	{
		LinkedList<Node> arreglo = new LinkedList<Node>();
		if(n < 2 || m < 2)
			throw new Exception("N y M tienen que ser mayores o iguales que 2");

		else
		{
			Queue<LatLng> intersecciones = new Queue<LatLng>();
			final Mapa example= new Mapa("test");

			double distColum = Math.abs((Math.abs(lonmax)- Math.abs(lonmin))/ (m-1));   
			double distFilas = (Math.abs(latmax)- Math.abs(latmin))/ (n-1);

			for(int i = 0; i < m;i++){
				LatLng pnt = new LatLng(latmin, lonmin+ i*distColum);
				LatLng pnt2 = new LatLng(latmax, lonmin+ i*distColum);
				example.generateSimplePath(pnt, pnt2);	
			}

			for(int j=0; j< n; j++)
			{
				LatLng pnt0 = new LatLng(latmin + j*distFilas, lonmin);
				LatLng pnt02 = new LatLng(latmin + j*distFilas, lonmax);
				example.generateSimplePath(pnt0, pnt02);
			}

			for(int k =0; k < n ; k++)
			{
				double fila = latmin + k*distFilas;
				for(int h=0; h <m;h++)
				{
					double columna = lonmin + h*distColum;
					LatLng pht = new LatLng(fila,columna);
					intersecciones.enqueue(pht);
				}
			}

			while(!intersecciones.isEmpty())
			{
				LatLng uu = intersecciones.dequeue();
				example.generateMarker(uu);
				Queue cola = dividir2(latmax, lonmax, latmin, lonmin);
				if(cola != null){

					Node elquees = darNodoDistanciaMinima(cola,uu.getLat(), uu.getLng());
					System.out.println("Vertice m�s cercano a : " + uu.toString() + " es " + elquees.toString() + "| con Id: " + elquees.getId());
					example.generateMarker(new LatLng(elquees.getLat(),elquees.getlon()));
					arreglo.add(elquees);
				}
				else
				{
					System.out.println(":C");
				}
			}
			
			System.out.println("n�mero de vertices: " + arreglo.getSize());
			Iterator<Node> itu = arreglo.iterator();
		}
		return arreglo;
	}

	public Queue<Node> dividir2(double latmax, double lonmax, double latmin, double lonmin)
	{
		SeparateChainingHash<String, Vertice> ts =  grafo.vertices();
		Iterator<String> k = ts.keys();
		Queue<Node> hh = new Queue<Node>();
		while(k.hasNext() )
		{
			Node node = grafo.getInfoVertex(k.next());
			if(node.getLat() <= latmax && node.getLat() >= latmin && node.getlon() <= lonmax && node.getlon() >= lonmin)
			{
				hh.enqueue(node);
			}
		}
		return hh;
	}
	/**
	 * Encuentra el nodo cuya distancia a una infracci�n es la m�nima comparada con los dem�s nodos de una cola
	 * @param cola, cola que contiene los nodos a comparar
	 * @param g, infracci�n con la que se va a comparar
	 * @return devuelve el nodo
	 */
	public  Node darNodoDistanciaMinima(Queue cola, double lat1, double lon1)
	{
		Node n = null;

		Iterator<Node> iter = cola.iterator();
		if(cola.size >0){
			n = iter.next();
			double lat2 = n.getLat();
			double lon2 = n.getlon();
			double distancia = calcularHaversine(lat1, lat2, lon1, lon2);
			while(iter.hasNext())
			{
				Node n2 = iter.next();
				double lat22 = n2.getLat();
				double lon22 = n2.getlon();
				double distancia2 = calcularHaversine(lat1, lat22, lon1, lon22);

				if(distancia2 < distancia)
				{
					n = n2;
					distancia = distancia2;
				}
			}
		}
		return n;
	}


	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 1C:  Calcular un �rbol de expansi�n m�nima (MST) con criterio distancia, utilizando el algoritmo de Kruskal.
	 * @return 
	 */
	public Queue<Arco> arbolMSTKruskalC1() {

		Queue<Arco> mst = new Queue<Arco>();

		
		Graph< String, Node, Way> subgrafo = new Graph<String, Node, Way>(grafo.V());
		
		Vertice act = mayor;
		
		
		 MaxHeapCP<Arco> pq = new MaxHeapCP<Arco>(grafo.V(), new ByDistance(grafo, false));
		 
		 Iterator iterador = grafo.arcos().keys();
		 
		 while(iterador.hasNext())
		 {
			 pq.agregar((Arco) iterador.next());
		 }
		 
		 while (!pq.esVacia() && mst.size() < grafo.V() - 1) {
	            Arco e = pq.delMax();
	            String v = (String) e.darVerticeIni();
	            String w = (String) e.darVerticeFin();
	           mst.enqueue(e);
	               
	
	        }

		 return mst;
	}
	
	
	        

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 2C: Calcular un �rbol de expansi�n m�nima (MST) con criterio distancia, utilizando el algoritmo de Prim. (REQ 2C)
	 */
	public void arbolMSTPrimC2() {
		// TODO Auto-generated method stub
		SeparateChainingHash<String, String> edgeTo = new SeparateChainingHash<String, String>(grafo.V());
		SeparateChainingHash<String, Integer> distTo = new SeparateChainingHash<String, Integer>(grafo.V());
		SeparateChainingHash<String, Boolean> marked = new SeparateChainingHash<String, Boolean>(grafo.V());
		 MaxHeapCP<Arco> pq = new MaxHeapCP<Arco>(grafo.V(), new ByDistance(grafo, false));
	    
	    SeparateChainingHash<String, Vertice> vertices = grafo.vertices();
		Iterator iterador = vertices.keys();
	    
	    while(iterador.hasNext())
		{
			String vertice = (String) iterador.next();
			
			distTo.put(vertice, Integer.MAX_VALUE);
			
			Iterator iter = grafo.vertices().keys();
			
			while(iter.hasNext())
			{
				String v = (String) iter.next();
				if (marked.get(v)==false)
				{
					distTo.put(v, 0);
		            Iterator<String> aux = grafo.adj(v);
		            marked.put(v, true);
					pq.agregar(grafo.vertices().get(v).getArco(aux.next())); 
					boolean primera = true;
				        while (!pq.esVacia()) {
				            if(primera)
				            {
				            	primera = false;
				            	
				            }else
				            {
				            	v = (String) pq.delMax().darVerticeFin();
					            marked.put(v, true);
					            aux = grafo.adj(v);

				            }
				            while (aux.hasNext()) {
				            	String w = aux.next();
				            	int dist = (int) calcularHaversine(grafo.getInfoVertex(v).getLat(), grafo.getInfoVertex(w).getLat(), grafo.getInfoVertex(v).getlon(), grafo.getInfoVertex(w).getlon());
				                
				                if (marked.get(w)) continue;         
				                if (dist < distTo.get(w)) {
				                    distTo.replace(w, dist);
				                    edgeTo.put(w, v);
				                    pq.agregar(grafo.vertices().get(v).getArco(w));
				                }
				            }
				        }
				}
			}
			
		}

	}
	
	public  double calcularHaversine(double lat1, double lat2, double lon1, double lon2)
	{
		double restaLat = lat2-lat1;
		double restaLon = lon2-lon1;
		double difLatitud =Math.toRadians(restaLat);
		double difLongitud= Math.toRadians(restaLon);

		double a = Math.pow(Math.sin(difLatitud/2),2) +
				Math.cos(Math.toRadians(lat1))*
				Math.cos(Math.toRadians(lat2))*
				Math.pow(Math.sin(difLongitud/2),2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		return c*6371.01;
	}
	

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 3C: Calcular los caminos de costo m�nimo con criterio distancia que conecten los v�rtices resultado
	 * de la aproximaci�n de las ubicaciones de la cuadricula N x M encontrados en el punto 5.
	 */
	public void caminoCostoMinimoDijkstraC3() {

	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 4C:Encontrar el camino m�s corto para un viaje entre dos ubicaciones geogr�ficas escogidas aleatoriamente al interior del grafo.
	 * @param idVertice2 
	 * @param idVertice1 
	 */
	public LinkedList<String> caminoMasCortoC4(String idVertice1, String idVertice2, int n) {
		// TODO Auto-generated method stub
		LinkedList<String> path = new LinkedList<String>();

		SeparateChainingHash<String, Double> distTo = new SeparateChainingHash<String, Double>(grafo.V());
		SeparateChainingHash<String, String> edgeTo = new SeparateChainingHash<String, String>(grafo.V());
		MaxHeapCP<String> pq = new MaxHeapCP<String>(grafo.V(), new ByNum(grafo, false));

		SeparateChainingHash<String, Vertice> ts =  grafo.vertices();
		Iterator<String> iterador = ts.keys();


		while(iterador.hasNext())
		{
			String vertice = iterador.next();
			distTo.put(vertice, Double.POSITIVE_INFINITY);

		}
		distTo.put(idVertice1, 0.0);


		pq.agregar(idVertice1);

		while (!pq.esVacia()) {
			String v = pq.delMax();

			Iterator<String> iter = grafo.adj(v);



			while(iter.hasNext())
			{

				String w =  (String)(grafo.vertices().get(v).getArco(iter.next()).darOtroVertice(v));
				Node w1 = grafo.getInfoVertex(v);
				Node h1 = grafo.getInfoVertex(w);
				Double harv = calcularHaversine(w1.getLat(), h1.getLat(), w1.getlon(), h1.getlon());
				Double peso = harv +n;
				if(distTo.get(w) == null)
					System.out.println(w);
				if (distTo.get(w) > (distTo.get(v) + peso)) {

					distTo.replace(w, (distTo.get(v) + peso));

					edgeTo.put(v, w);
					pq.agregar(w);
				}

			}


			boolean cond = true;

			for (String h = ""+idVertice2; cond && h!=null; h = edgeTo.get(h)) {
				if(h.equals(""+idVertice1))
				{
					cond = false;
				}else{

					path.add(h);
				}
			}
			if(n==0)
			{
				graficarCamino(path,idVertice1,idVertice2);
				double acumr = 0.0;
				Iterator<String> uuu = path.iterator();
				String uu1 = uuu.next();
				while(uuu.hasNext())
				{
					String uu2 = uuu.next();
					Node ahhh = grafo.getInfoVertex(uu1);
					Node ahh2 = grafo.getInfoVertex(uu2);
					double acum = calcularHaversine(ahhh.getLat(), ahh2.getLat(), ahhh.getlon(), ahh2.getlon());
					acumr += acum;
					System.out.println("Id : " + uu1 + ", Total de infracciones: " + ahhh.getInfraccionesCercanas().getSize() + ", Distancia acumulada: " + acumr);
					uu1 = uu2;
				}
			}
		}
		return path;
	}
	

	public void req8c(LinkedList<Node> nodos)
	{
		LinkedList<LinkedList<String>> areh = new LinkedList<LinkedList<String>>();
		Iterator<Node> hh = nodos.iterator();
		Node act = hh.next();
		while(hh.hasNext())
		{
			Node act1 = hh.next();
			areh.add(caminoMasCortoC4(act.getId(), act1.getId(), 1));
		}
		
		Iterator<LinkedList<String>> itareh = areh.iterator();
		double j =0.0;
		int i =0;
		while(itareh.hasNext())
		{
			System.out.println("Camino num "+ i);
			LinkedList<String> acti = itareh.next();
			graficarCamino(acti,null,null);
			double acumr = 0.0;
			Iterator<String> uuu = acti.iterator();
			String uu1 = uuu.next();
			while(uuu.hasNext())
			{
				String uu2 = uuu.next();
				Node ahhh = grafo.getInfoVertex(uu1);
				Node ahh2 = grafo.getInfoVertex(uu2);
				double acum = calcularHaversine(ahhh.getLat(), ahh2.getLat(), ahhh.getlon(), ahh2.getlon());
				acumr += acum;
				System.out.println("Id : " + uu1 );
				uu1 = uu2;
			}
			System.out.println("Distancia en km: " + acumr);
			if(acumr > j)
			{
				j= acumr;
			}
			i++;
		}
		System.out.println("La distancia m�s grande fue :"+ j);
	}
}