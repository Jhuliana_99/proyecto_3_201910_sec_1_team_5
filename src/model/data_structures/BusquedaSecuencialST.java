package model.data_structures;

import java.util.Iterator;

import reader.Cuadrante;

public class BusquedaSecuencialST<K extends Comparable<K>,V> {

	/**
	 * Representa al primer nodo de la lista encadenada
	 */
	private Node first; 

	/**
	 * Tama�o de la lista
	 */
	private int size = 0;

	/**
	 * Clase que representa cualquier nodo de la lista.
	 */
	private class Node
	{ 
		K key;
		V val;
		Node next;
		public Node(K key, V val, Node next)
		{
			this.key = key;
			this.val = val;
			this.next = next;
		}

		public K getKey()
		{
			return key;
		}

		public Node getNext()
		{
			return next;
		}

	}

	/**
	 * Retorna un elemento igual al buscado.
	 * @param el, elemento buscado.
	 * @return el elemento buscado en caso de que exista, null en caso contrario.
	 */
	public V get(K el)
	{ 
		for (Node x = first; x != null; x = x.next){
			if(el instanceof Cuadrante)
			{
				Cuadrante ele = (Cuadrante)el;
				Cuadrante ele2 = (Cuadrante)x.key;
				String n = Double.toString(ele.getLatInf())+ Double.toString(ele.getlatSup())+ Double.toString(ele.getLonInf())+ Double.toString(ele.getlonSup());
				String n2 = Double.toString(ele2.getLatInf())+ Double.toString(ele2.getlatSup())+ Double.toString(ele2.getLonInf())+ Double.toString(ele2.getlonSup());
				if(n.equals(n2))
				{
					return x.val;
				}

			}
			else if (el.equals(x.key))
				return x.val; 
		}
		return null; 
	}

	/**
	 * Agrega un elemento a la lista encadenada, si el elemento ya existe y su valor es una busquedaSecuencialST
	 * agrega el elemento en la busquedaSecuencial, de lo contrario reemplaza el valor del elemento por el valor agregado.a
	 * @param el, representa al elemento que quiere ser agregado.
	 * @param val, el valor de dicho elemento.
	 */
	public int put(K el, V val)
	{ 
		for (Node x = first; x != null; x = x.next){
			if(el instanceof Cuadrante)
			{
				Cuadrante ele = (Cuadrante)el;
				Cuadrante ele2 = (Cuadrante)x.key;
				String n = Double.toString(ele.getLatInf())+ Double.toString(ele.getlatSup())+ Double.toString(ele.getLonInf())+ Double.toString(ele.getlonSup());
				String n2 = Double.toString(ele2.getLatInf())+ Double.toString(ele2.getlatSup())+ Double.toString(ele2.getLonInf())+ Double.toString(ele2.getlonSup());
				if(n.equals(n2))
				{
					if(val instanceof Queue){
						concat((Queue)x.val,(Queue)val);
						return 0;
					}
					else {
						x.val = val; 
						return 0;
					} 
				}
			}
			else if (el.equals(x.key))
			{ 
				if(val instanceof Queue){
					concat((Queue)x.val,(Queue)val);
					return 0;
				}
				else {
					x.val = val; 
					return 0;
				} 
			}
		}

		first = new Node(el, val, first); 
		size++;
		return 1;
	} 

	/**
	 * Solo permite agregar elementos repetidos
	 */
	public void putEspecial(K el, V val)
	{ 
		for (Node x = first; x != null; x = x.next){
			if (el.equals(x.key))
			{ 
				x.val = val; 
			}
		}
	} 


	public void delete(K key)
	{
		for (Node x = first; x != null; x = x.next)
			if (key.equals(x.key)){
				x.val = null; x.key = null; 
				size--;
			}
	}

	/**
	 * @return el tama�o de la lista encadenada.
	 */
	public int size()
	{
		return size;
	}

	/**
	 * Concatena una nueva lista con esta.
	 * @param list2 lista a la que se quiere concatenar
	 */
	/**
	 * Concatena dos colas
	 * @param q1 cola a la que se quiere concatenar
	 * @param q2 cola que se quiere concatenar
	 */
	public void concat( Queue q1, Queue q2)
	{
		Object el = q2.dequeue();

		while(el != null)
		{
			q1.enqueue(el);
			el = q2.dequeue();

		}

	}

	/**
	 * 
	 * @return
	 */
	public Iterator<K> keys()
	{
		Iterator<K> t = new Iterator<K>() {

			private int currentIndex = 0;
			Node currentElement = first;

			@Override
			public boolean hasNext() {
				return currentIndex < size && currentElement != null;
			}

			@Override
			public K next() {
				Node h = currentElement;
				currentElement = h.getNext();
				currentIndex++;
				return h.getKey();			
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		return t;
	}
}
