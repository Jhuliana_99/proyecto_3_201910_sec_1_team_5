package model.data_structures;

import java.util.Iterator;

public class BFS<V extends Comparable <V>>{

	private SeparateChainingHash<V,Boolean> marked; 
	private SeparateChainingHash<V,V> edgeTo; 
	private final V s; // source

	public BFS(Graph G, V s)
	{
		marked = new SeparateChainingHash<V,Boolean>(G.V());
		edgeTo = new SeparateChainingHash<V,V>(G.V());
		this.s = s;
		bfs(G, s);
	}

	public SeparateChainingHash<V,V> bfs(Graph G, V s)
	{
		Queue<V> queue = new Queue<V>();

		marked.put(s, true); 
		queue.enqueue(s); 

		while (!queue.isEmpty())
		{
			V v = queue.dequeue();

			Iterator<Arco> yy = G.adj(v);
			while(yy.hasNext()){
				V w = (V)((Arco)yy.next()).darOtroVertice(v);


				if (marked.get(w) == null) 
				{
					edgeTo.put(w, v); 
					marked.put(w, true); 
					queue.enqueue(w); 
				}

			}
		}
		return null;
	}

	public boolean hasPathTo(V v)
	{ 
		if( marked.get(v) != null)
			return marked.get(v);
		else
			return false;

	}

	public LinkedList<V> getPath(V u,V v)
	{
		LinkedList<V> rta = new LinkedList<V>();
		rta.add(v);
		V el = v;
		while(!el.equals(u)){
			el = edgeTo.get(el);
			rta.add(el);
		}
		return rta;
	}



}
