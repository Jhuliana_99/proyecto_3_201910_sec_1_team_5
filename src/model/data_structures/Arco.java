package model.data_structures;

/**
 * Clase que modela un arco de un grafo.
 *
 * @param <A> Clase generica que modela la informacion del arco
 */
public class Arco <A, K> implements Comparable<Arco <A, K>>{

	
	//Atributos
	
	/**
	 * id del Vertice inicial del arco.
	 */
	private K verticeIni;
	
	/**
	 * id del Vertice final del arco.
	 */
	private K verticeFin;
	
	/**
	 * Informacion del arco.
	 */
	private A info;
	
	
	//Constructor
	
	/**
	 * Contructor de un arco.
	 * @param idVertexIni id del Vertice inicial del arco
	 * @param idVertexFin id del Vertice final del arco
	 * @param infoArc informacion del arco
	 */
	public Arco(K idVertexIni, K idVertexFin, A infoArc) {
		
		verticeIni = idVertexIni;
		
		verticeFin = idVertexFin;
		
		info = infoArc;
		
	}
	
	//Metodos
	

	public K darOtroVertice(K vertice){
	
			return verticeFin;
	}
	

	
	/**
	 * Retorna la informacion del arco
	 * @return A info del arco
	 */
	public A darInfo(){
		return info;
	}

	
	/**
	 * Cambia la informacion por la dada por parametro.
	 * @param infoArc informacion nueva.
	 */
	public void cambiarInfo(A infoArc) {

		info = infoArc;
	}

	@Override
	public int compareTo(Arco o) {
		
		if((o.verticeFin.equals(this.verticeFin) && o.verticeIni.equals(this.verticeIni)) || (o.verticeFin.equals(this.verticeIni)&& o.verticeIni.equals(this.verticeFin)))
		return 0;
		
		else return -1;
	}

	public K darVerticeFin() {
		// TODO Auto-generated method stub
		return verticeFin;
	}

	public Object darVerticeIni() {
		// TODO Auto-generated method stub
		return verticeIni;
	}
	

}
